import { rp } from '../utils'

const request = user => ({
  method: 'GET',
  uri: `http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=7D5F2FA02FF09ACA687DE979BE355B30&steamid=${user}&include_played_free_games=1`,
})

export const ownGames = async(users) => {
  const games = await Promise.all(users.map(async(user) => {
    try {
      const res = await rp(request(user))
      const body = JSON.parse(res.body).response
      return body.games
    } catch(e){
      console.log(e)
    }
  }))

  return games
}
