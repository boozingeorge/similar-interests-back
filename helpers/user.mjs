import { rp } from '../utils'

const request = user => ({
  method: 'GET',
  uri: `http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001?key=7D5F2FA02FF09ACA687DE979BE355B30&vanityurl=${user}`,
})

export const vanityUrls = users =>
  users.map(async(user) => {
    if (db.users[user]) return db.users[user]

    const res = await rp(request(user))
    const body = JSON.parse(res.body).response
    if (body.success === 1) {
      db.users[user] = body.steamid
      return body.steamid
    }

    return { error: body.success }
  })
