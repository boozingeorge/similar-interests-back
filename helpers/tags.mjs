import { rp } from '../utils'

const request = game => ({
  method: 'GET',
  uri: `http://steamspy.com/api.php?request=appdetails&appid=${game.appid}`,
})

export const multiplayerGames = async(games) => {
  const gamesWithTags = await Promise.all(games.map(async(game) => {
    try {
      const res = await rp(request(game))
      const body = JSON.parse(res.body)
      return { appid: body.appid, name: body.name, tags: body.tags }
    } catch(e) {
      console.log(e)
    }
  }))

  return gamesWithTags.reduce((acc, game) => {
    if(game.tags && game.tags.Multiplayer) {
      acc.push(game.name)
      return acc
    }
    return acc
  }, [])
}

