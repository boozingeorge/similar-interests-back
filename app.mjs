import Koa from 'koa'
import bodyParser from 'koa-bodyparser'

import routers from './routers'
import {connect} from './db'
import { rp } from './utils'
const app = new Koa()

const PORT = 3000

app.use(bodyParser({ enableTypes: ['form', 'json', 'text'] }))


app.use(routers.routes())
app.use(routers.allowedMethods())

connect().then(() => {
  app.listen(PORT, () => console.log(`Similar Interests server on ${PORT} port`))
})
