import Router from 'koa-router'
import intersectionBy from 'lodash.intersectionby'
import { vanityUrls as getVanityUrls  } from './helpers/user'
import { ownGames as getOwnGames } from './helpers/game'
import { multiplayerGames as getMultiplayerGames } from './helpers/tags'

const router = new Router()

const haveIncorrectUsers = users => users.reduce((acc, user, index) => {
  if(typeof user  === 'object') {
    acc.decision = true
    acc.users.push(index)
  }
  return acc
}, { decision: false, users: [] })

const handleIncorrectUser = (incorrectUsers, users) => ({
  error: {
    code: 1,
    users: incorrectUsers.users.map(index => ({ index, name: users[index] }))
  }
})

const similarGames = async(ctx, next) => {
  const { users } = ctx.request.body
  try {
    const users_ = await Promise.all(getVanityUrls(users))
    const incorrectUsers = haveIncorrectUsers(users_)
    if(incorrectUsers.decision) return ctx.body = handleIncorrectUser(incorrectUsers, users)

    const ownGames = await getOwnGames(users_)
    const intersection = intersectionBy(...ownGames, 'appid')
    const multiplayerGames = await getMultiplayerGames(intersection)

    ctx.body = { games: multiplayerGames }
  } catch(e) {
    console.log(e)
  }
}

router.post('/similar-games', similarGames)

export default router

// code 0 - ok
// code 1 - bad name
// code 2 - WTF
